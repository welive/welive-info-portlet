<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
String contextPath = request.getContextPath();
String imgPath = contextPath + "/img";
%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
String casLoginUrl = "/c/portal/login";
boolean login = false;
try {
	if(user == null || !themeDisplay.isSignedIn()) {
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(request);
		if("true".equals(httpRequest.getParameter("login"))) {
			login = true;
		}
	}
} catch(Exception e) {}
%>

<c:choose>
	
	<c:when test="<%=login%>">
		<script>
			location.replace("<%=casLoginUrl%>");
		</script>
	</c:when>
	<c:otherwise>

		<div id="materialize-body" class="materialize">
			<div class="container">
				<div class="section center">
					<img src="<%=imgPath%>/logo_welive.png" alt="WeLive Project">
				</div>
				<div class="section center">
					<h5 class="header grey-text text-darken-4"><liferay-ui:message key="info.title" /></h5>
				</div>
				<div class="section">
					<liferay-ui:message key="info.paragraph.1" />
				</div>
				<div class="divider"></div>
				<div class="section">
					<liferay-ui:message key="info.paragraph.2" />
				</div>
				<div class="divider"></div>
				<div class="section">
					<liferay-ui:message key="info.paragraph.3" />
				</div>
				<div class="divider"></div>
				<div class="section">
					<liferay-ui:message key="info.paragraph.4" />
				</div>
				<div class="divider"></div>
				<div class="section">
					<liferay-ui:message key="info.paragraph.5" />
				</div>
				<div class="divider"></div>
				<div class="section">
					<liferay-ui:message key="info.paragraph.6" />
				</div>
			</div>
		</div>
	
	</c:otherwise>
</c:choose>
